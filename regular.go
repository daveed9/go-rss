package rss

import (
	"encoding/xml"
	"net/http"

	"github.com/paulrosania/go-charset/charset"
)

//Channel struct for RSS
type Channel struct {
	Title         string `xml:"title"`
	Link          string `xml:"link"`
	Description   string `xml:"description"`
	Language      string `xml:"language"`
	LastBuildDate Date   `xml:"lastBuildDate"`
	Item          []Item `xml:"item"`
}

//ItemEnclosure struct for each Item Enclosure
type ItemEnclosure struct {
	URL  string `xml:"url,attr"`
	Type string `xml:"type,attr"`
}

//Item struct for each Item in the Channel
type Item struct {
	Author      string          `xml:"author"`
	Category    []string        `xml:"category"`
	Comments    string          `xml:"comments"`
	Content     string          `xml:"content"`
	Creator     string          `xml:"creator"`
	Description string          `xml:"description"`
	Enclosure   []ItemEnclosure `xml:"enclosure"`
	FullText    string          `xml:"full-text"`
	GUID        string          `xml:"guid"`
	Link        string          `xml:"link"`
	PubDate     Date            `xml:"pubDate"`
	Title       string          `xml:"title"`
}

//Regular parses regular feeds
func Regular(resp *http.Response) (*Channel, error) {
	defer resp.Body.Close()
	xmlDecoder := xml.NewDecoder(resp.Body)
	xmlDecoder.CharsetReader = charset.NewReader

	var rss struct {
		Channel Channel `xml:"channel"`
	}
	if err := xmlDecoder.Decode(&rss); err != nil {
		return nil, err
	}
	return &rss.Channel, nil
}
